﻿using UnityEngine;
using UnityEngine.Assertions;

using System.Collections;
using System.Collections.Generic;

[AddComponentMenu("")]
public class CubeSpawner : MonoBehaviour
{
	[SerializeField]
	private Transform CubePrefab;

	[SerializeField]
	private float Interval = 0.3f;
	
	[SerializeField]
	private Tracker m_Tracker;

	List<Transform> m_InstantiatedObjects = new List<Transform>();

	float m_ElapsedTimeSinceLastSpawn;

	void Start()
	{
		Assert.IsNotNull(m_Tracker);
	}

	void Update()
	{
		for (int i = m_InstantiatedObjects.Count - 1; i >= 0; i--)
		{
			if (transform.TransformPoint(m_InstantiatedObjects[i].position).y < -0.25f ||
			    m_InstantiatedObjects[i].localScale.magnitude < 0.001f)
			{
				Destroy(m_InstantiatedObjects[i].gameObject);
				m_InstantiatedObjects.RemoveAt(i);
			}
			else
			{
				m_InstantiatedObjects[i].localScale *= 0.975f;
			}
		}

		if (m_Tracker.isMouthOpen())
		{
			m_ElapsedTimeSinceLastSpawn += Time.deltaTime;

			if (m_ElapsedTimeSinceLastSpawn > Interval)
			{
				SpawnCube();
				m_ElapsedTimeSinceLastSpawn = 0.0f;
			}
		}

	}

	void SpawnCube()
	{
		var t = Instantiate(CubePrefab, transform.position, Random.rotationUniform);
		var rb = t.GetComponent<Rigidbody>();
		rb.velocity = new Vector3(Random.Range(-0.5f, 0.5f), 
								  Random.Range(-0.5f, 0.5f), 
								  -1.0f * Random.Range(1.0f, 4.0f));
		m_InstantiatedObjects.Add(t);
	}
}
