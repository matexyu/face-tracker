using UnityEngine;
using UnityEngine.Assertions;

using System;
using System.IO;
using System.Diagnostics;

public class GifRecordingController : MonoBehaviour
{
    readonly bool kShowGui = true;
    readonly bool kDoLog = false; 

	private Moments.Recorder m_Recorder;

    [SerializeField]
    private Tracker m_VisageTracker;

    [SerializeField]
    private String m_GifSaveSubfolder = "FaceTrackerRecordedGifs";

    private bool m_IsSaving;
    private float m_Progress;
    private String m_LastFile;

    Stopwatch m_Stopwatch = new Stopwatch();

    void Start()
    {
        Assert.IsNotNull(m_VisageTracker);

        m_Recorder = GetComponent<Moments.Recorder>();
        Assert.IsNotNull(m_Recorder);

        Assert.IsNotNull(m_GifSaveSubfolder);

        if (m_GifSaveSubfolder != "")
        {
            String save_to_subfolder = "";

#if UNITY_EDITOR
            // If we are running under the Unity Editor, save the images outside of the Assets folder
            save_to_subfolder = Path.Combine(m_Recorder.SaveFolder, "./../" + m_GifSaveSubfolder);
#else
            String documents_path = Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments);
            save_to_subfolder = Path.Combine(documents_path, m_GifSaveSubfolder);
#endif

            Directory.CreateDirectory(save_to_subfolder);
            m_Recorder.SaveFolder = save_to_subfolder;            
        }

		// Optional callbacks (see each function for more info).
		m_Recorder.OnPreProcessingDone = OnProcessingDone;
		m_Recorder.OnFileSaveProgress = OnFileSaveProgress;
		m_Recorder.OnFileSaved = OnFileSaved;
    }

	void OnProcessingDone()
	{
		// All frames have been extracted and sent to a worker thread for compression !
		// The Recorder is ready to record again, you can call Record() here if you don't
		// want to wait for the file to be compresse and saved.
		// Pre-processing is done in the main thread, but frame compression & file saving
		// has its own thread, so you can save multiple gif at once.

		m_IsSaving = true;
	}

	void OnFileSaveProgress(int id, float percent)
	{
		// This callback is probably not thread safe so use it at your own risks.
		// Percent is in range [0;1] (0 being 0%, 1 being 100%).
		m_Progress = percent * 100f;
	}

	void OnFileSaved(int id, string filepath)
	{
		// Our file has successfully been compressed & written to disk !
		m_LastFile = filepath;
		m_IsSaving = false;
	}

	void OnDestroy()
	{
		// Memory is automatically flushed when the Recorder is destroyed or (re)setup,
		// but if for some reason you want to do it manually, just call FlushMemory().
		//m_Recorder.FlushMemory();
	}

    bool m_didCallRecord;

    void Update()
    {
        if (m_Recorder.State == Moments.RecorderState.Paused &&
            m_VisageTracker.isMouthOpen() &&
            m_didCallRecord == false)
        {
            if (kDoLog) print("Now calling Record() ...");
			m_Recorder.Record();
            m_Stopwatch.Start();

            m_didCallRecord = true;
        }

        if (m_didCallRecord)
        {
            double elapsed_time_seconds = m_Stopwatch.ElapsedMilliseconds / 1000.0;

            if (kDoLog) print("Recording time seconds: " + elapsed_time_seconds);

            if (elapsed_time_seconds > 3.0)
            {
                if (kDoLog) print("Now calling Save() ...");
                m_Recorder.Save();
                m_Progress = 0f;

                m_Stopwatch.Stop();
                m_Stopwatch.Reset();

                m_didCallRecord = false;
            }
        }


    }

    //----------------

    void OnGUI()
    {
        if (kShowGui == false)
        {
            return;
        }

        GUILayout.BeginHorizontal();
        GUILayout.Space(10f);
        GUILayout.BeginVertical();

        GUILayout.Space(10f);
        GUILayout.Label("Recorder State : " + m_Recorder.State.ToString());

        if (m_IsSaving)
            GUILayout.Label("Progress Report : " + m_Progress.ToString("F2") + "%");

        if (string.IsNullOrEmpty(m_LastFile) == false)
            GUILayout.Label("Last File Saved at: '" + m_LastFile + "'");

        GUILayout.EndVertical();
        GUILayout.EndHorizontal();
    }
}
