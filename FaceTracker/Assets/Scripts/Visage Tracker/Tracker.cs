using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.IO;

/** Enum used for tracking status.
 */
public enum TrackStatus
{
	Off = 0,
	Ok = 1,
	Recovering = 2,
	Init = 3
}

public enum FaceEffect
{
	Glasses = 0,
	Tiger = 1
}


/** Class that implements the behaviour of tracking application.
 * 
 * This is the core class that shows how to use visage|SDK capabilities in Unity. It connects with visage|SDK through calls to native methods that are implemented in VisageTrackerUnityPlugin.
 * It uses tracking data to transform objects that are attached to it in ControllableObjects list.
 */
public class Tracker : MonoBehaviour
{
#region Properties

	/** Tracker config file in editor.
	 */
#if !UNITY_EDITOR
	[HideInInspector]
#endif
	public string ConfigFileEditor;

	/** Tracker config file name on standalone platforms.
	 */
#if !UNITY_STANDALONE_WIN
	[HideInInspector]
#endif
	public string ConfigFileStandalone;

	/** Tracker config file on iOS.
	 */
#if !UNITY_IPHONE
	[HideInInspector]
#endif
	public string ConfigFileIOS;

	/** Tracker config file on Android.
	 */
#if !UNITY_ANDROID
	[HideInInspector]
#endif
	public string ConfigFileAndroid;

	/** Tracker config file on Mac.
	 */
#if !UNITY_STANDALONE_OSX
	[HideInInspector]
#endif
	public string ConfigFileOSX;

#if UNITY_ANDROID
	private AndroidJavaObject androidCameraActivity;
#endif
#if !UNITY_STANDALONE_WIN || !UNITY_EDITOR
	[HideInInspector]
#endif
	public string licenseFileFolder;

#if !UNITY_STANDALONE_OSX && !UNITY_ANDROID && !UNITY_IPHONE
	[HideInInspector]
#endif

	public string licenseFileName;

	public Transform[] ControllableObjects;

	private Vector3[] startingPositions;
	private Vector3[] startingRotations;

	//Mesh data
	public const int MaxVertices = 1024;
	public const int MaxTriangles = 2048;
	public int VertexNumber;
	public int TriangleNumber;

	[HideInInspector]
	public Vector2[] TexCoords;
	[HideInInspector]
	public Vector3[] Vertices;
	[HideInInspector]
	public int[] Triangles;

	private float[] vertices = new float[MaxVertices * 3];
	private int[] triangles = new int[MaxTriangles * 3];
	private float[] texCoords = new float[MaxVertices * 2];
	private MeshFilter meshFilter;
	public TextAsset TexCoordinatesFile;
	private Vector2[] modelTexCoords; 

	//
	public Material CameraViewMaterial;
	public Vector3 Translation;
	public Vector3 Rotation;
	public float Focus;
	public int ImageWidth = 360;
	public int ImageHeight = 480;
	public int TexWidth = 512;
	public int TexHeight = 512;
	private Texture2D texture = null;
	private Color32[] texturePixels;
	private GCHandle texturePixelsHandle;
	public bool isTracking = false;
	public int TrackerStatus = 0;

    [Header("Camera settings")]
    public int Orientation = 0;
    private int currentOrientation = 0;
	public int isMirrored = 1;
	public int device = 0;
    private int currentDevice = 0;
    public int defaultCameraWidth = -1;
    public int defaultCameraHeight = -1;

    GUIContent contentSwitchCam = new GUIContent ();
	GUIContent contentStartTracking = new GUIContent ();
	GUIContent contentStopTracking = new GUIContent ();
	GUIContent contentSwitchEffect = new GUIContent();

    [Header("Buttons")]
    public Texture2D imageSwitchCam;
	public Texture2D imageStartTracking;
	public Texture2D imageStopTracking;
	public Texture2D imageSwitchEffect;
	private bool trackButton;
	private bool stopTrackButton;
	private bool switchCamButton;
	private bool effectButton;
	private FaceEffect currentEffect = FaceEffect.Tiger;
   
	private bool AppStarted = false;

	private GUIStyle startTrackingStyle = null;
	private GUIStyle stopTrackingStyle = null;
	private GUIStyle switchCamButtonStyle = null;
	private GUIStyle switchCamEffectStyle = null;

    AndroidJavaClass unity;


    #endregion

	//-----------------------------------------------------
	[SerializeField]
	private Material lineMaterial;
	private float m_mouthOpeningMillimeters;
	private float m_OverallEyeOpeningMillimeters;
	private Vector3 m_mouthCenter;

	public float getMouthOpeningMillimeters()
	{
		return m_mouthOpeningMillimeters;
	}

	public Vector3 getMoutCenter()
	{
		return m_mouthCenter;
	}

	public float getOverallEyeOpeningMillimeters()
	{
		return m_OverallEyeOpeningMillimeters;
	}

	public bool isMouthOpen()
	{
		return (m_mouthOpeningMillimeters > 15.0f);
	}

	public bool areEyesOpen()
	{
		return (m_OverallEyeOpeningMillimeters > 5.0f);
	}
	//-----------------------------------------------------

    void Start ()
	{
		// switch cam image
		contentSwitchCam.image = (Texture2D)imageSwitchCam;
		contentStartTracking.image = (Texture2D)imageStartTracking;
		contentStopTracking.image = (Texture2D)imageStopTracking;
		contentSwitchEffect.image = (Texture2D)imageSwitchEffect;

		startTrackingStyle = new GUIStyle ();
		startTrackingStyle.normal.background = (Texture2D)contentStartTracking.image;
		startTrackingStyle.active.background = (Texture2D)contentStartTracking.image;
		startTrackingStyle.hover.background = (Texture2D)contentStartTracking.image;
		
		stopTrackingStyle = new GUIStyle ();
		stopTrackingStyle.normal.background = (Texture2D)contentStopTracking.image;
		stopTrackingStyle.active.background = (Texture2D)contentStopTracking.image;
		stopTrackingStyle.hover.background = (Texture2D)contentStopTracking.image;

		switchCamButtonStyle = new GUIStyle ();
		switchCamButtonStyle.normal.background = (Texture2D)contentSwitchCam.image;
		switchCamButtonStyle.active.background = (Texture2D)contentSwitchCam.image;
		switchCamButtonStyle.hover.background = (Texture2D)contentSwitchCam.image;

		switchCamEffectStyle = new GUIStyle();
		switchCamEffectStyle.normal.background = (Texture2D)contentSwitchEffect.image;
		switchCamEffectStyle.active.background = (Texture2D)contentSwitchEffect.image;
		switchCamEffectStyle.hover.background = (Texture2D)contentSwitchEffect.image;

		// get mesh filter component
		meshFilter = GetComponent<MeshFilter>();

		// create a new mesh
		 meshFilter.mesh = new Mesh();

		// get model texture coordinates
		modelTexCoords = GetTextureCoordinates();

		Translation = new Vector3 (0, 0, 0);
		Rotation = new Vector3 (0, 0, 0);

		// choose config file
		string configFilePath = Application.streamingAssetsPath + "/" + ConfigFileStandalone;
		string licenseFilePath = Application.streamingAssetsPath + "/" + licenseFileFolder;
		
		switch (Application.platform) {
			
		case RuntimePlatform.IPhonePlayer: 
			configFilePath = "Data/Raw/Visage Tracker/" + ConfigFileIOS;
			licenseFilePath = "Data/Raw/Visage Tracker/" + licenseFileName;
			break;
		case RuntimePlatform.Android: 
			configFilePath = Application.persistentDataPath + "/" + ConfigFileAndroid;
			licenseFilePath = Application.persistentDataPath + "/" + licenseFileName;
			break;
		case RuntimePlatform.OSXPlayer: 
			configFilePath = Application.dataPath + "/Resources/Data/StreamingAssets/Visage Tracker/" + ConfigFileOSX;
			licenseFilePath = Application.dataPath + "/Resources/Data/StreamingAssets/Visage Tracker/" + licenseFileName;
			break;
		case RuntimePlatform.OSXEditor: 
			configFilePath = Application.dataPath + "/StreamingAssets/Visage Tracker/" + ConfigFileOSX;
			licenseFilePath = Application.dataPath + "/StreamingAssets/Visage Tracker/" + licenseFileName;
			break;
		case RuntimePlatform.WindowsEditor:
			configFilePath = Application.streamingAssetsPath + "/" + ConfigFileEditor;
			licenseFilePath = Application.streamingAssetsPath + "/" + licenseFileFolder;
			break;
		}

		// initialize tracker
		InitializeTracker (configFilePath, licenseFilePath);
		
		// check orientation and start cam
		Orientation =GetDeviceOrientation ();

		OpenCamera (Orientation, device, defaultCameraWidth, defaultCameraHeight, isMirrored);

		startingPositions = new Vector3[ControllableObjects.Length];
		startingRotations = new Vector3[ControllableObjects.Length];

		for (int i = 0; i < ControllableObjects.Length; i++) {
			startingPositions [i] = ControllableObjects [i].transform.position;
			startingRotations [i] = ControllableObjects [i].transform.rotation.eulerAngles;
		}

		isTracking = true;

		if (SystemInfo.graphicsDeviceType == UnityEngine.Rendering.GraphicsDeviceType.OpenGLCore)
			Debug.Log ("Notice: if graphics API is set to OpenGLCore, the texture might not get properly updated.");
	}

	void OnGUI ()
	{	
		GUI.Label(new Rect(0, 0, Screen.width, Screen.height / 6),
			"Mouth mm = "  + (int)m_mouthOpeningMillimeters + 
			"  Eyes mm = " + (int)m_OverallEyeOpeningMillimeters);
		return;

		if (!isTracking) {	
			if (ImageWidth < ImageHeight)
				trackButton = GUI.Button (new Rect (0, Screen.height - Screen.width / 7, Screen.width / 8, Screen.width / 8), " ", startTrackingStyle);
			else
				trackButton = GUI.Button (new Rect (0, Screen.height - Screen.height / 7, Screen.height / 8, Screen.height / 8), " ", startTrackingStyle);


			if (trackButton)
				isTracking = true;
		}

		if (isTracking) {
			if (ImageWidth < ImageHeight)
				stopTrackButton = GUI.Button (new Rect (0, Screen.height - Screen.width / 7, Screen.width / 8, Screen.width / 8), " ", stopTrackingStyle);
			else
				stopTrackButton = GUI.Button (new Rect (0, Screen.height - Screen.height / 7, Screen.height / 8, Screen.height / 8), " ", stopTrackingStyle);


			if (stopTrackButton) {
				isTracking = false;
			}
		}
			
		if (ImageWidth < ImageHeight)
			switchCamButton = GUI.Button (new Rect (Screen.width - Screen.width / 6, Screen.height - Screen.width / 6, Screen.width / 6, Screen.width / 6), " ", switchCamButtonStyle);
		else
			switchCamButton = GUI.Button (new Rect (Screen.width - Screen.height / 6, Screen.height - Screen.height / 6, Screen.height / 6, Screen.height / 6), " ", switchCamButtonStyle);

		if (switchCamButton)
			currentDevice = (currentDevice == 1) ? 0 : 1;

	   
		if (ImageWidth < ImageHeight)
		{
			effectButton = GUI.Button(
				new Rect(Screen.width - Screen.width / 6, 
						 Screen.height / 30, 
						 Screen.width  / 8, 
						 Screen.width  / 8), 
						 " ", switchCamEffectStyle);
		}
		else
		{
			effectButton = GUI.Button(
				new Rect(Screen.width - Screen.height / 6, 
						 Screen.width  / 30, 
						 Screen.height / 6, 
						 Screen.height / 6),
						  " ", switchCamEffectStyle);
		}

		if (effectButton)
			currentEffect = ((currentEffect == FaceEffect.Glasses) ? FaceEffect.Tiger : FaceEffect.Glasses);

	}

	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Escape)) {
			Application.Quit ();
		}
		
		
	#if (UNITY_IPHONE || UNITY_ANDROID) && UNITY_EDITOR
		// no tracking on ios while in editor
		return;
	#endif

		if (isTracking) {

			// find device orientation
			currentOrientation = GetDeviceOrientation ();

			// check if orientation or camera device changed
			if (currentOrientation != Orientation || currentDevice != device) 
			{
				OpenCamera (currentOrientation, currentDevice, defaultCameraWidth, defaultCameraHeight, isMirrored);
				Orientation = currentOrientation;
				device = currentDevice;
				texture = null;
			}
				
			// grab current frame and start face tracking

			VisageTrackerNative._grabFrame ();

			TrackerStatus = VisageTrackerNative._track ();

						//
			// Compute mouth opening amount from MPEG-4 Feature Points
			//
			{
				const int numPoints = 2 + 2 + 2; // Lips, Left Eye, Right Eye
				int[] groups  = {2,2, 3,3, 3,3};
				int[] indices = {2,3, 1,3, 2,4};

				float[] pointCoordinates = new float[numPoints * 3];
				Vector3[] points = new Vector3[numPoints];

				VisageTrackerNative._getFeaturePoints3D(numPoints, groups, indices, pointCoordinates);
	
				for (int i = 0; i < numPoints; i++) 
				{
					points [i] = new Vector3 (pointCoordinates [i * 3 + 0], 
					                             pointCoordinates [i * 3 + 1], 
												 pointCoordinates [i * 3 + 2]);
				}

				// Mouth
				m_mouthOpeningMillimeters = 1000.0f * 
				Vector3.Distance(points[0], points[1]);

				// Mouth mid-point
				m_mouthCenter = Vector3.Lerp(points[0], points[1], 0.5f);

				// Eyes opening
				m_OverallEyeOpeningMillimeters = 1000.0f * (
					Vector3.Distance(points[2], points[3]) +
					Vector3.Distance(points[4], points[5])
				);
			}

			// update tracker status and translation and rotation
			VisageTrackerNative._get3DData (
				out Translation.x, 
				out Translation.y, 
				out Translation.z, 
				out Rotation.x,
				out Rotation.y, 
				out Rotation.z);

			for (int i = 0; i < ControllableObjects.Length; i++)
			{
				ControllableObjects[i].transform.position = startingPositions[i] + Translation;
				ControllableObjects[i].transform.rotation = Quaternion.Euler(startingRotations[i] + Rotation);
			}

			this.transform.position = Translation;
			this.transform.rotation = Quaternion.Euler (Rotation);

#if UNITY_ANDROID
            VisageTrackerNative._getCameraInfo(out Focus, out ImageWidth, out ImageHeight);

            //waiting for information about frame width and height
            if (ImageWidth == 0 || ImageHeight == 0)
                return;

         
#else
            VisageTrackerNative._getCameraInfo (out Focus, out ImageWidth, out ImageHeight);
#endif

			float aspect = ImageWidth / (float)ImageHeight;

			float yRange = (ImageWidth > ImageHeight) ? 1.0f : 1.0f / aspect;

			Camera.main.fieldOfView = Mathf.Rad2Deg * 2.0f * Mathf.Atan (yRange / Focus);

			//------------------------------------------------------------------------
			//  Face Model
			//------------------------------------------------------------------------

			VisageTrackerNative._getFaceModel (out VertexNumber, vertices, out TriangleNumber, triangles, texCoords);

			// vertices
			if (Vertices.Length != VertexNumber)
				Vertices = new Vector3[VertexNumber];

			for (int i = 0; i < VertexNumber; i++) {
				Vertices [i] = new Vector3 (vertices [i * 3 + 0], vertices [i * 3 + 1], vertices [i * 3 + 2]);
			}

			// triangles
			if (Triangles.Length != TriangleNumber)
				Triangles = new int[TriangleNumber * 3];

			for (int i = 0; i < TriangleNumber * 3; i++) {
				Triangles [i] = triangles [i];
			}

			// tex coords
			if (TexCoords.Length != VertexNumber)
				TexCoords = new Vector2[VertexNumber];

			for (int i = 0; i < VertexNumber; i++) {
				TexCoords[i] = new Vector2(modelTexCoords[i].x, modelTexCoords[i].y); //new Vector2 (texCoords [i * 2 + 0], texCoords [i * 2 + 1]);
			}

		}

		RefreshImage();

		// create mesh

		meshFilter.mesh.Clear();
		if (currentEffect == FaceEffect.Tiger)
		{		
			/*	
			for (int i = 0; i < ControllableObjects.Length; i++)
			{
				if (ControllableObjects[i].gameObject.activeInHierarchy == false) continue;
				
				//ControllableObjects[i].transform.position -= new Vector3(0,0,10000);
			}*/

			meshFilter.mesh.vertices = Vertices;
			meshFilter.mesh.triangles = Triangles;
			meshFilter.mesh.uv = TexCoords;
			meshFilter.mesh.uv2 = TexCoords;
			//meshFilter.mesh.Optimize();
			meshFilter.mesh.RecalculateNormals();
			meshFilter.mesh.RecalculateBounds();
		}
	}
		

	void OnDestroy ()
	{
#if UNITY_ANDROID
        this.androidCameraActivity.Call("closeCamera");
#else
        VisageTrackerNative._closeCamera ();
#endif
	}


	/** This method initializes the tracker.
	 */
	bool InitializeTracker (string config, string license)
	{
		Debug.Log ("Visage Tracker: Initializing tracker with config: '" + config + "'");

#if (UNITY_IPHONE || UNITY_ANDROID) && UNITY_EDITOR
		return false;
#endif

#if UNITY_ANDROID

		Shader shader = Shader.Find("Unlit/Texture");
		CameraViewMaterial.shader = shader;

		// initialize visage vision
		VisageTrackerNative._loadVisageVision();
		Unzip();

		unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		this.androidCameraActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
#else
		Shader shader = Shader.Find("Custom/BGRATex");
		CameraViewMaterial.shader = shader;
#endif
        // initialize tracker
        VisageTrackerNative._initTracker(config, license);
		return true;
	}


	// returns current device orientation
	int GetDeviceOrientation ()
	{
		int devOrientation;

		//Device orientation is obtained in AndroidCameraPlugin so we only need information about whether orientation is changed
#if UNITY_ANDROID
		int oldWidth = ImageWidth;
		int oldHeight = ImageHeight;
		VisageTrackerNative._getCameraInfo(out Focus, out ImageWidth, out ImageHeight);

		if ((oldWidth!=ImageWidth || oldHeight!=ImageHeight) && ImageWidth != 0 && ImageHeight !=0 && oldWidth != 0 && oldHeight !=0 )
			devOrientation = (Orientation ==1) ? 0:1;
		else
			devOrientation = Orientation;
#else

		if (Input.deviceOrientation == DeviceOrientation.Portrait)
			devOrientation = 0;
		else if (Input.deviceOrientation == DeviceOrientation.PortraitUpsideDown)
			devOrientation = 2;
		else if (Input.deviceOrientation == DeviceOrientation.LandscapeLeft)
			devOrientation = 3;
		else if (Input.deviceOrientation == DeviceOrientation.LandscapeRight)
			devOrientation = 1;
		else if (Input.deviceOrientation == DeviceOrientation.FaceUp)
			devOrientation = Orientation;
		else 
			devOrientation = 0;
#endif

		return devOrientation;
	}

	// if width and height are -1, values will be set internally
	void OpenCamera (int orientation, int currDevice, int width, int height, int mirrored)
	{
#if UNITY_ANDROID
        if (device == currDevice && AppStarted)
			return;
		
		//camera needs to be opened on main thread
        this.androidCameraActivity.Call("runOnUiThread", new AndroidJavaRunnable(() => {
            this.androidCameraActivity.Call("closeCamera");
            this.androidCameraActivity.Call("GrabFromCamera", width, height, currDevice);
        }));
        AppStarted = true;
#else
		VisageTrackerNative._openCamera (orientation, currDevice, width, height, mirrored);
#endif

	}

	void RefreshImage ()
	{
		// create texture
		if (texture == null && isTracking) {
			TexWidth = Convert.ToInt32 (Math.Pow (2.0, Math.Ceiling (Math.Log (ImageWidth) / Math.Log (2.0))));
			TexHeight = Convert.ToInt32 (Math.Pow (2.0, Math.Ceiling (Math.Log (ImageHeight) / Math.Log (2.0))));
#if UNITY_ANDROID
			texture = new Texture2D (TexWidth, TexHeight, TextureFormat.RGB24, false);
#else
			texture = new Texture2D (TexWidth, TexHeight, TextureFormat.RGBA32, false);
#endif
			
			var cols = texture.GetPixels32();
            for (var i = 0; i < cols.Length; i++)
				cols[i] = Color.black;
			
			texture.SetPixels32(cols);
			texture.Apply(false);
			
			
			
#if UNITY_STANDALONE_WIN
			// "pin" the pixel array in memory, so we can pass direct pointer to it's data to the plugin,
			// without costly marshaling of array of structures.
			texturePixels = ((Texture2D)texture).GetPixels32(0);
			texturePixelsHandle = GCHandle.Alloc(texturePixels, GCHandleType.Pinned);
#endif
			
		}

		if (texture != null && isTracking && TrackerStatus != 0) {
			
#if UNITY_STANDALONE_WIN
			CameraViewMaterial.SetTexture("_MainTex", texture);

			// send memory address of textures' pixel data to VisageTrackerUnityPlugin
			VisageTrackerNative._setFrameData(texturePixelsHandle.AddrOfPinnedObject());
			((Texture2D)texture).SetPixels32(texturePixels, 0);
			((Texture2D)texture).Apply();
			
#elif UNITY_IPHONE || UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX || UNITY_ANDROID
			CameraViewMaterial.SetTexture ("_MainTex", texture);
			

			if (SystemInfo.graphicsDeviceVersion.StartsWith ("Metal"))
				VisageTrackerNative._bindTextureMetal (texture.GetNativeTexturePtr ());
			else
				VisageTrackerNative._bindTexture ((int)texture.GetNativeTexturePtr ());
			
#endif
		} 
	}

	void Unzip()
	{
		string[] pathsNeeded = {
			"candide3.fdp",
			"candide3.wfm",
			"jk_300.fdp",
			"jk_300.wfm",
			"Head Tracker.cfg",
            "visage_powered.png",
			"warning.png",
			"bdtsdata/FF/ff.dat",
			"bdtsdata/LBF/lv",
			"bdtsdata/LBF/pr.lbf",
			"bdtsdata/NN/fa.lbf",
			"bdtsdata/NN/fc.lbf",
			"bdtsdata/LBF/pe/landmarks.txt",
			"bdtsdata/LBF/pe/W",
			"bdtsdata/LBF/pe/lp11.bdf",
			"bdtsdata/LBF/ye/lp11.bdf",
			"bdtsdata/LBF/ye/W",
			"bdtsdata/LBF/ye/landmarks.txt"
			, "license-file-name.vlc"
		};
		string outputDir;
		string localDataFolder = "Visage Tracker";
		
		StreamWriter sw;

		outputDir = Application.persistentDataPath;
		
		if (!Directory.Exists(outputDir)) 
		{
			Directory.CreateDirectory(outputDir);
		}
		foreach(string filename in pathsNeeded)
		{
			//if(!File.Exists(outputDir + "/" + filename))
			//{
			
			WWW unpacker = new WWW("jar:file://" + Application.dataPath + "!/assets/" + localDataFolder + "/" + filename);
			
			while(!unpacker.isDone){ }
			
			if(!string.IsNullOrEmpty(unpacker.error))
			{
				Debug.Log(unpacker.error);
				continue;
			}
			
			Debug.Log(filename);
			
			if (filename.Contains("/")) 
			{
				string[] split = filename.Split('/');
				string name = "";
				string folder = "";
				string curDir = outputDir;
				
				for (int i = 0; i < split.Length; i++) 
				{
					if (i == split.Length - 1)
					{
						name = split[i];
					}
					else 
					{
						folder = split[i];
						curDir = curDir + "/" + folder;                    
					}
				}
				if (!Directory.Exists(curDir))
				{
					Directory.CreateDirectory(curDir);
				}
				
				File.WriteAllBytes("/" + curDir + "/" + name, unpacker.bytes);
			}
			else
			{
				File.WriteAllBytes("/" + outputDir + "/" + filename, unpacker.bytes);
			}
			
			Debug.Log("File written " + filename + "\n");
		}
	}


	// .obj file parsing (normals excluded)
	Vector2[] GetTextureCoordinates()
	{
		List<Vector3> v = new List<Vector3>();
		List<Vector2> vt = new List<Vector2>();
		Vector2[] texCoords = new Vector2[1024];
		int indexPoint;
		int texIndexPoint;

		string text = TexCoordinatesFile.text;
		string[] lines = text.Split(new[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);

		foreach (string line in lines)
		{
			string[] lineSplit = line.Split(' ');

			// ex. "v -22.571495 -26.737658 38.676575"             
			if (lineSplit[0] == "v")
			{
				v.Add(new Vector3((float)Double.Parse(lineSplit[1]), float.Parse(lineSplit[2]), float.Parse(lineSplit[3])));
			}

			// ex. "vt 0.5465 0.2624"
			else if (lineSplit[0] == "vt")
			{
				vt.Add(new Vector2(float.Parse(lineSplit[1]), float.Parse(lineSplit[2])));
			}

			// ex. "f 410/410/413 399/399/413 63/60/413"
			else if (lineSplit[0] == "f")
			{
				int[] indexPoints = { 0, 0, 0 };
				int[] texIndexPoints = { 0, 0, 0 };
				for (int i = 1; i < 4; i++)
				{
					indexPoints[i - 1] = int.Parse(lineSplit[i].Split('/')[0]);
					texIndexPoints[i - 1] = int.Parse(lineSplit[i].Split('/')[1]);
				}

				for (int i = 0; i < 3; i++)
				{
					indexPoint = indexPoints[i];
					texIndexPoint = texIndexPoints[i];

					texCoords[indexPoint - 1].x = vt[texIndexPoint - 1].x;
					texCoords[indexPoint - 1].y = vt[texIndexPoint - 1].y;
				}

			}
		}

		return texCoords;
	}

	//=================================================================================
	
    void OnPostRender() 
    {
        Camera current_camera = Camera.current;

		if (current_camera == null)
		{
			return;
		}
        /*
        if (!mat) {
            Debug.LogError("Please Assign a material on the inspector");
            return;
        }*/
        GL.PushMatrix();
        //mat.SetPass(0);
		GL.modelview = current_camera.worldToCameraMatrix;
        GL.LoadProjectionMatrix(current_camera.projectionMatrix);
		GL.Viewport(new Rect(0, 0, Screen.width, Screen.height));

        DrawGL();

        GL.PopMatrix();
/*
        GL.Begin(GL.TRIANGLES);
        GL.Color(new Color(1, 1, 1, 1));
        GL.Vertex3(0.5F, 0.25F, 0);
        GL.Vertex3(0.25F, 0.25F, 0);
        GL.Vertex3(0.375F, 0.5F, 0);
        GL.End();
        GL.Begin(GL.QUADS);
        GL.Color(new Color(0.5F, 0.5F, 0.5F, 1));
        GL.Vertex3(0.5F, 0.5F, 0);
        GL.Vertex3(0.5F, 0.75F, 0);
        GL.Vertex3(0.75F, 0.75F, 0);
        GL.Vertex3(0.75F, 0.5F, 0);
        GL.End();*/


    }

    private void DrawGL()
    {
        if (meshFilter == null)
        {
			print("No Mesh Filter");
            return;
        }

        Mesh mesh = meshFilter.sharedMesh;

        //Draw Face Normals
        {
            int[] triangles = mesh.triangles;
            Vector3[] vertices = mesh.vertices;
            
            int num_triangles = triangles.Length;
			if (lineMaterial != null)
			{
				lineMaterial.SetPass(0);
			}	

			GL.Color(new Color(1, 1, 0, 1));
            GL.Begin(GL.LINES);
            
            for (int i = 0; i < num_triangles; i += 3)
            {
                Vector3 v0 = transform.TransformPoint(vertices[triangles[i]]);
                Vector3 v1 = transform.TransformPoint(vertices[triangles[i + 1]]);
                Vector3 v2 = transform.TransformPoint(vertices[triangles[i + 2]]);
                Vector3 center = (v0 + v1 + v2) / 3;

                Vector3 dir = Vector3.Cross(v1 - v0, v2 - v0);
                dir /= dir.magnitude;

                //_faceNormals[i].Draw(center, dir);
                
                GL.Vertex3(center.x, center.y, center.z);
                GL.Vertex3(center.x + dir.x, 
                           center.y + dir.y, 
                           center.z + dir.z);
            }
            GL.End();
        }

    }

}
