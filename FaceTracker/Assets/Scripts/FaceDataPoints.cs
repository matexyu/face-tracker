/// Based on VisageRendering.cs DisplayFeaturePoints()
class FaceDataPoints
{
    public static readonly int[] innerLipPoints = {
            2,  2,
            2,  6,
            2,  4,
            2,  8,
            2,  3,
            2,  9,
            2,  5,
            2,  7,
        };

    public static readonly int[] outerLipPoints = {
            8,  1,
            8,  10,
            8,  5,
            8,  3,
            8,  7,
            8,  2,
            8,  8,
            8,  4,
            8,  6,
            8,  9,
        };

    public static readonly int[] nosePoints = {
            9,  1,
            9,  2,
            9,  3,
            9,  15
        };

    public static readonly int[] irisPointsRightEye = {
            3,  6
        };

    public static readonly int[] irisPointsLeft = {
            3,  5
        };

    public static readonly int[] eyesPointsR = {
            3,  2,
            3,  4,
            3,  8,
            3,  10,
            3,  12,
            3,  14,
            12, 6,
            12, 8,
            12, 10,
            12, 12
        };

    public static readonly int[] eyesPointsL = {
            3,  1,
            3,  3,
            3,  7,
            3,  9,
            3,  11,
            3,  13,
            12, 5,
            12, 7,
            12, 9,
            12, 11
        };

    public static readonly int[] eyebrowPoints = {
            4,  1,
            4,  2,
            4,  3,
            4,  4,
            4,  5,
            4,  6,
            14, 1,
            14, 2,
            14, 3,
            14, 4
        };

    // visible contour
    public static readonly int[] contourPointsVisible = {
            13, 1,
            13, 3,
            13, 5,
            13, 7,
            13, 9,
            13, 11,
            13, 13,
            13, 15,
            13, 17,
            13, 16,
            13, 14,
            13, 12,
            13, 10,
            13, 8,
            13, 6,
            13, 4,
            13, 2
        };
}
