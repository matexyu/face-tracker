﻿using UnityEngine;
using UnityEngine.Assertions;

namespace RDSystem
{
    public class RDSystemUpdater : MonoBehaviour
    {
        [SerializeField] MeshRenderer m_VideoMeshRenderer;
        [SerializeField] CustomRenderTexture _texture;
        [SerializeField, Range(1, 16)] int _stepsPerFrame = 4;

        [SerializeField] Tracker m_Tracker;

        void Start()
        {
            Assert.IsNotNull(m_VideoMeshRenderer);
            Assert.IsNotNull(m_Tracker);
            _texture.Initialize();

            foreach(string s in _texture.material.shaderKeywords)
            {
                print("Shader keyword: " + s);
            }
        }

        void Update()
        {
            float k_mouth = Mathf.Clamp((m_Tracker.getMouthOpeningMillimeters() - 5.0f) / 30.0f, 0.0f, 1.0f);

            //
            // Reinitialize if eyes became closed
            //
            bool did_reinit = false;

            if (m_Tracker.isTracking)
            {
                bool are_eyes_open = m_Tracker.areEyesOpen();

                if (are_eyes_open == false && m_AreEyesOpenOld == true)
                {
                    float dv = Random.Range(0.2f, 0.35f);
                    print("Setting dv to " + dv);
                    _texture.material.SetFloat("_Dv", dv);

                    _texture.Initialize();

                    did_reinit = true;
                }

                m_AreEyesOpenOld = are_eyes_open;
            }

            // Update simulation
            if (did_reinit == false)
            {
                float feed = k_mouth * 0.07f + 0.03f;
                _texture.material.SetFloat("_Feed", feed);

                int update_steps = (int)(k_mouth * 40.0f) + 4;
                _texture.Update(update_steps);
            }

        }

        bool m_AreEyesOpenOld;
    }
}
