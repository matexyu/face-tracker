========================================
  Face Tracking Experiment

  Mathieu Bosi, 2017/10/23 
  Developed in about 16 hours.

  Contact: mathieu.bosi@gmail.com
========================================

----------------
 Usage
----------------

Just run FaceTracker.exe and enjoy :)

When your mouth is open, a 3 seconds animated GIF will be recorded in a sub-folder called 'FaceTrackerRecordedGifs'
- When running from the Unity Editor, the GIFs base folder will be the project one
- When running the standalone executable, the root folder will be the user's Documents folder

The mouth opening controls the evolution of a Reaction-Diffusion procedural texture.
When closing the eyes, the computation will be reset.
Also, some pink cubes will pop out of your head / mind :)

A Direct X 11 capable graphics card is needed for the shader to work.

The key Scripts are contained in the 'Assets/Scripts' folder.

--------------------------
 Used resources + notes
--------------------------

* * *
Visage Tracker
http://visagetechnologies.com/

A 30 days trial license file is already included in the archive. Will work on up to 10 distinct machines.
The VisageTrackerNative.Windows.cs has been modified to expose the MPEG-4 feature points needed to compute mouth and eyes opening.

* * *
Keijiro's Reaction-Diffusion Shader
https://github.com/keijiro/RDSystem

The shader has been modified so that the generative texture has transparency for better blending with the underneath video texture.
The simulation is partially mapped to mouth opening and reset when eyes are closed.

* * *
Moments GIF Capture library by Thomas Hourdel (Unity Technologies)
https://github.com/Chman/Moments/

