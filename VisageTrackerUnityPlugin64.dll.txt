Microsoft (R) COFF/PE Dumper Version 14.00.24215.1
Copyright (C) Microsoft Corporation.  All rights reserved.


Dump of file VisageTrackerUnityPlugin64.dll

File Type: DLL

  Section contains the following exports for VisageTrackerUnityPlugin64.dll

    00000000 characteristics
    59DCDF5B time date stamp Tue Oct 10 16:55:23 2017
        0.00 version
           1 ordinal base
          19 number of functions
          19 number of names

    ordinal hint RVA      name

          1    0 00001870 _bindTexture
          2    1 00001670 _closeCamera
          3    2 00001880 _get3DData
          4    3 00001E40 _getActionUnitCount
          5    4 00001E70 _getActionUnitName
          6    5 00001EC0 _getActionUnitUsed
          7    6 00001F20 _getActionUnitValues
          8    7 00001C40 _getCameraInfo
          9    8 00001C90 _getFaceModel
         10    9 000020D0 _getFeaturePoints2D
         11    A 000021D0 _getFeaturePoints3D
         12    B 000022F0 _getFeaturePoints3DRel
         13    C 00002080 _getGazeDirection
         14    D 00001260 _grabFrame
         15    E 00001730 _initTracker
         16    F 00001300 _openCamera
         17   10 00001810 _releaseTracker
         18   11 000010E0 _setFrameData
         19   12 00001B70 _track

  Summary

        2000 .data
        1000 .gfids
        1000 .pdata
        4000 .rdata
        1000 .reloc
        1000 .rsrc
        7000 .text
        1000 .tls
